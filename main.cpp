#include "keccakmain.h"

int main(int argc, char **argv)
{
	Keccakmain*keccakmain=new Keccakmain(new Keccak,new Common);

	if(!strcmp(argv[1],"-h")){
		keccakmain->hash_message(argv[2]);
	}else if(!strcmp(argv[1],"-f")){
		keccakmain->hash_file(argv[2]);
	}else if(!strcmp(argv[1],"-u")){
		cout<<"keccak -h <message> <message_length> to hash a message"<<endl<<endl<<endl;
		cout<<"keccak -f <file_name> to hash a file"<<endl<<endl<<endl;
	}

	delete keccakmain;
 
    return 0;
}
