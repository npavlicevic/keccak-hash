#include "keccakmain.h"

Keccakmain::Keccakmain(Keccak*keccak,Common*common){
	this->keccak=keccak;
	this->common=common;
}

Keccakmain::~Keccakmain(){
	delete this->keccak;
	delete this->common;
}

void Keccakmain::hash_message(char*message){
	int _l_message_length;

	_l_message_length=strlen(message);

	if(_l_message_length<=136){
		cout<<"Message"<<endl<<endl;
		cout<<message<<endl<<endl;

		cout<<"Message length"<<endl<<endl;
		cout<<_l_message_length<<endl<<endl;

		memcpy(this->buffer,message,_l_message_length);

		this->keccak->keccak_final(this->buffer,_l_message_length,32);

		this->keccak->get_hash_bytes(this->hash,32);

		cout<<"Message hash"<<endl<<endl;
		this->common->show_hex(this->hash,32);
	}
}

void Keccakmain::hash_file(char*file_name){
	unsigned int block_size_bytes=this->keccak->get_block_size_bytes();
	unsigned int bytes_read;
	unsigned char*buffer;
	FILE*file;

	buffer=(unsigned char*)malloc(block_size_bytes);

	long int start_time;
	long int time_difference;
	struct timespec gettime_now;

	clock_gettime(CLOCK_REALTIME,&gettime_now);
	start_time=gettime_now.tv_nsec;

	file=fopen(file_name,"rb");

	while((bytes_read=fread(buffer,1,block_size_bytes,file))==block_size_bytes){
		this->keccak->keccak_hash_block(buffer,bytes_read,32);
	}

	this->keccak->keccak_final(buffer,bytes_read,32);

	clock_gettime(CLOCK_REALTIME,&gettime_now);
	time_difference=gettime_now.tv_nsec-start_time;

	cout<<"Time difference"<<endl<<endl;
	cout<<time_difference<<endl<<endl;

	this->keccak->get_hash_bytes(this->hash,32);

	cout<<"Message hash"<<endl<<endl;
	this->common->show_hex(this->hash,32);

	fclose(file);

	free (buffer);
}
