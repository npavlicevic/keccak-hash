#ifndef KECCAK_H
#define KECCAK_H

#include <stdint.h>
#include <string.h>

#ifndef KECCAK_ROUNDS
#define KECCAK_ROUNDS 24
#endif

#ifndef ROTL64
#define ROTL64(x, y) (((x) << (y)) | ((x) >> (64 - (y))))
#endif

class Keccak
{
private:
	static const uint64_t keccakf_rndc[24];
	static const int keccakf_rotc[24];
	static const int keccakf_piln[24];
	uint64_t state[25];
	static const unsigned int block_size_bytes;

public:
	Keccak();
	// update the state
	void keccakf(uint64_t st[25], int norounds);

	// compute a keccak hash (md) of given byte length from "in"
	int keccak_hash_block(const uint8_t *in,int inlen,int mdlen);

	// compute final hash
	int keccak_final(
		const uint8_t *in,
		int inlen,
		int mdlen
	);

	void get_hash_bytes(uint8_t*hash,int hashlen);

	unsigned int get_block_size_bytes(){
		return block_size_bytes;
	}
};

#endif
