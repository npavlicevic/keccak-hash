#include <iomanip>
#include <iostream>
#include <fstream>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include "common.h"
#include "keccak.h"

using namespace std;

class Keccakmain{
protected:
	Keccak*keccak;
	Common*common;
	unsigned char buffer[64];
	unsigned char hash[32];
public:
	Keccakmain(Keccak*keccak,Common*common);
	~Keccakmain();

	void hash_message(char*message);
	void hash_file(char*file_name);
};
